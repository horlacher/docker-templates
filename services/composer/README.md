# docker-templates / composer

## Dockerfile
### Install Composer in PHP image
Only necessary to install composer for a specific PHP version or on edge cases when it's required inside a webserver.

Copy from composer image
```
COPY --from=composer /usr/bin/composer /usr/bin/composer
```
Or download and install from getcomposer.org
```
RUN apt-get install -y curl && \
  curl -sS https://getcomposer.org/installer | php \
  && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer
```