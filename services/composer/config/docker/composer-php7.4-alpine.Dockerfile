FROM php:7.4-alpine
WORKDIR /var/www/html

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# PHP modules
RUN apk add libzip-dev libpng-dev icu-dev
RUN docker-php-ext-install zip calendar gd intl