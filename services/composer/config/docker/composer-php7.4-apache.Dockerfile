FROM php:7.4-apache
WORKDIR /var/www/html

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# update & system tools
RUN apt-get update

# composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# PHP modules
RUN apt-get install -y libzip-dev zlib1g-dev libpng-dev libicu-dev
RUN docker-php-ext-install zip calendar gd intl

# clean up the image
RUN rm -rf /var/lib/apt/lists/*