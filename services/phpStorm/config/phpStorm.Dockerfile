FROM ubuntu:focal
WORKDIR /root

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# install inotify and libnss for PhpStorm
RUN apt-get update && apt-get -y install inotify-tools libnss3-dev libgdk-pixbuf2.0-dev libgtk-3-dev libxss-dev

# add tools
RUN apt-get -y install git util-linux openssh-client wget
