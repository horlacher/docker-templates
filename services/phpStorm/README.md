# docker-templates / phpStorm on WSL 2

**this is work in progress**

# The problem

Docker file access on Windows (WSL 2 with the files on a NTFS filesystem) is very slow  and running file access intensive applications like npm will be way too slow to work with.
The issue is discussed in [this bug report](https://github.com/microsoft/WSL/issues/4197) and a performance comparison documented [here](https://vxlabs.com/2019/12/06/wsl2-io-measurements/).

# PhpStorm in docker

This is an approach to move all project files into a named docker volume and run PhpStorm inside docker, which allows docker applications to access files with much higher speeds.

## Install X-Server on Windows
On Windows systems, displaying the graphical application requires an x-server running on the host like [VcXsrv](https://sourceforge.net/projects/vcxsrv/).

## Create a named docker volume
Create the volume on the host:
```
docker volume create --name PhpstormProjects
```

## Install PhpStorm

Enter the console of the PhpStorm container.
```
docker exec -it phpstorm sh
```

Get the direct link for the latest linux release on the [PhpStorm download page](https://www.jetbrains.com/phpstorm/download/#section=linux) and download it from within the container.
```
wget https://download.jetbrains.com/webide/PhpStorm-2021.3.2.tar.gz # <- replace with latest 
tar -xzf PhpStorm-*.tar.gz -C /opt
ln -s /opt/PhpStorm-*/bin/phpstorm.sh /bin/phpstorm
```

