FROM node:8
WORKDIR /var/www/html

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# update & system tools
RUN apt-get update \
    && apt-get -y install git

# npm
RUN npm install bower gulp -g

# clean up the image
RUN rm -rf /var/lib/apt/lists/*

EXPOSE 80
