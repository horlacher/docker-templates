FROM alpine
WORKDIR /var/www/html

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# add tools
RUN apk add git util-linux openssh-client

EXPOSE 80
