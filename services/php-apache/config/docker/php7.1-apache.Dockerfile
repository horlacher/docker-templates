FROM php:7.1-apache
WORKDIR /var/www/html

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Zurich
RUN ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# update & system tools
RUN apt-get update \
    && apt-get install -y lynx less curl iputils-ping

# PHP DB drivers
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql

# PHP xdebug
RUN pecl install xdebug-2.9.0 && docker-php-ext-enable xdebug
# pecl/xdebug requires PHP (version >= 7.2.0, version <= 8.0.99)
#RUN pecl install xdebug && docker-php-ext-enable xdebug

# apache mods
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf &&\
    a2enmod rewrite &&\
    service apache2 restart

# install PHP mail
RUN apt-get install -y sendmail &&\
    echo "sendmail_path=/usr/sbin/sendmail -t -i" >> /usr/local/etc/php/conf.d/sendmail.ini &&\
    sed -i '/#!\/bin\/sh/aservice sendmail restart' /usr/local/bin/docker-php-entrypoint &&\
    sed -i '/#!\/bin\/sh/aecho "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts' /usr/local/bin/docker-php-entrypoint

# Forward mails to mailhog
# (unser) Laravel verwendet kein sendmail, also in der Konfig tmpl-mailhog:1025 hinterlegen und danach "php artisan config:cache" ausführen
RUN curl --location --output /usr/local/bin/mhsendmail https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64 && chmod +x /usr/local/bin/mhsendmail &&\
    echo 'sendmail_path="/usr/local/bin/mhsendmail --smtp-addr=mail.tmpl.test:1025 --from=mail@docker.local"' > /usr/local/etc/php/conf.d/mailhog.ini

# apache document root
ENV APACHE_DOCUMENT_ROOT /var/www/html/web
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf &&\
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# clean up the image
RUN rm -rf /var/lib/apt/lists/*

EXPOSE 80
