# docker-templates / php-apache

## Dockerfile
### PHP
#### PHP modules
DB drivers:
```
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql
```
Some modules require system packages and need to be enabled:
```
RUN apt-get install -y libzip-dev zlib1g-dev libpng-dev libicu-dev
RUN docker-php-ext-install zip calendar gd intl opcache && docker-php-ext-enable opcache
```
#### install xdebug
```
RUN pecl install xdebug && docker-php-ext-enable xdebug
```
Install a specific xdebug version:
```
RUN pecl install xdebug-2.9.0 && docker-php-ext-enable xdebug
```
Xdebug needs to be configured in `php.ini`

Keep in mind there were many configuration parameter changes between v2 and v3:
https://xdebug.org/docs/upgrade_guide

#### Allow xdebug to pass firewall
If you are blocking incoming connections to your host with ufw, you'll need to allow docker machines to connect on port 9003 (respectively 9000 for xdebug v2)
```
sudo ufw allow in from 172.16.0.0/12 to any port 9000 comment xDebug9000
sudo ufw allow in from 172.16.0.0/12 to any port 9003 comment xDebug9003
```

#### composer
Better use a separate service for composer, see service composer