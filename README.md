# docker-templates

This repo is a reference for building docker images for php web development.

## fix www-data permissions
`chmod a+rX -R www-data && chmod -R a+w  www-data/upload`

## Commands
### Start/stop service
* Start server `docker-compose up [container_name]`
* Stop server `docker-compose stop [container_name]`

### Force build image
`docker-compose build --no-cache *container_name*`

### Enter docker shell
`docker exec -t -i *container_name* /bin/bash`

### List running containers
`docker ps`

## best practices
* Don't bind services from docker-compose.yaml `command` argument to avoid connection problems like ERR_CONNECTION_RESET
* Bind servers on 0.0.0.0 (for IPv4) to allow it to be reachable from outside the container (opposed to localhost)

## Dockerfile images
### apt-get
First call update to retrieve available packages.
Use argument `-y` to confirm installation in advance. Clean up lists after to keep the image slim.  
```
RUN apt-get update \
&& apt-get install -y less

# clean up the image
RUN rm -rf /var/lib/apt/lists/*
```
### alpine
Alpine images are not debian based and use `apk` instead of `apt`. Available packages: https://pkgs.alpinelinux.org/packages
```
RUN apk add libzip-dev libpng-dev icu-dev
```

### Debug
#### Tools
Install tools `ping` (iputils-ping), `telnet`, `curl`, `lynx`  
```
RUN apt-get update \
&& apt-get install -y lynx curl iputils-ping
```

#### Netstat
Call this from host:
* get pid: `docker inspect -f '{{.State.Pid}}' *container_name*`
* exec nettstat: `sudo nsenter -t *pid* -n netstat`


## docker-compose.yaml
### run commands

Keep service awake without having a server application:
```
command: tail -f /dev/null
```
Multiple commands:
```
command: bash -c "composer update && composer install"
```
Multiple commands multiline:
```
command: >
      bash -c "cd web/app/themes/tmpl
      && npm install
      && tail -f /dev/null"
```
Multiple commands for alpine:
```
command: sh -c "composer update && composer install"
```
Run services from within Dockerfile:
```
CMD /usr/sbin/apache2ctl -D FOREGROUND
```